import React, { Component } from 'react'

export default class Ccomponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://api.openweathermap.org/data/2.5/onecall?lat=50.45&lon=30.52&appid=bdb1612d695419c1c42de7b6a8daf462")
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    items: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }


    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <p> Error {error.message} </p>
        } else if (!isLoaded) {
        return <p> Loading... </p>
        } else {
            return (

               <div className='block'>
                    
                    <div className='block__title'>
                        <h1 className='text-align'>Weather forecast</h1>
                        <h2 className='text-align'>{items.timezone}</h2>
                    </div>

                    <div className='block__weather'>

                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[0].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[0].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[0].temp - 273.15)} °C</p>
                        </div>

                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[1].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[1].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[1].temp - 273.15)} °C</p>
                        </div>
                        
                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[2].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[2].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[2].temp - 273.15)} °C</p>
                        </div>

                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[3].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[3].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[3].temp - 273.15)} °C</p>
                        </div>

                        <div className='block__item'>
                            < img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[4].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[4].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[4].temp - 273.15)} °C</p>
                        </div>

                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[5].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[5].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[5].temp - 273.15)} °C</p>
                        </div>

                        <div className='block__item'>
                            <img className='block__icon' src={'http://openweathermap.org/img/w/' + items.hourly[6].weather[0].icon + '.png'}  alt='icon'/>
                            <p className='block__text'>{items.hourly[6].weather[0].main}</p>
                            <p className='block__text'>{Math.round(items.hourly[6].temp - 273.15)} °C</p>
                        </div>

                    </div>
               
                </div>
            )
        }
    }
}