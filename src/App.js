import React from 'react';
import './App.css';
import Ccomponent from './Ccomponent';
import { PieChart } from './PieChart';

function App() {
  return (
    <Ccomponent />,
    <PieChart />
  );
}

export default App;
